topButton = document.getElementById("top-button");

window.onscroll = () => scrollFunction();

scrollFunction = () => {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20)
    topButton.style.display = "block";
  else topButton.style.display = "none";
};

topButton.onclick = () => {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
};
